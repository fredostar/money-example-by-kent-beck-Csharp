﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tdd_money
{
    [TestClass]
    public class UnitTest1
    {
        private Dollar five;

        [TestInitialize]
        public void avantChaqueTest()
        {
             five = new Dollar(5);
        }

        [TestMethod]
        public void TestMultiplication()
        {
            Assert.AreEqual(10, five.times(2));

        }

        [TestMethod]
        public void TestEquals()
        {
            Assert.IsTrue(five.equals(new Dollar(5)));
            Assert.IsFalse(five.equals(new Dollar(6)));
        }
        
    }
}
